% genano.tex $ this file belongs to the Molcas repository $

\section{\program{genano}}
\label{UG:sec:genano}
\index{Program!Genano@\program{Genano}}\index{Genano@\program{Genano}}
%%Description:
%%+This program is used to construct ANO type basis sets.
\program{GENANO} is a program for
determining the contraction coefficients for
generally contracted basis sets \cite{Raffenetti:73}.
They are determined by diagonalizing a density matrix,
using the eigenvectors (natural orbitals) as
the contraction coefficients, resulting
in basis sets of the ANO (Atomic Natural Orbitals)
type \cite{Almlof:87}.\index{ANO}\index{Atomic Natural Orbitals}

Some elementary theory: We can do a spectral resolution of a density matrix $D$
\begin{equation}
\label{eqn:d-spectral}
D=\sum_k \eta_k c_k c_k^{\dag}
\end{equation}
where $\eta_k$ is the k'th eigenvalue (occupation value)
and $c_k$ is the k'th eigenvector (natural orbital).
The occupation number for a natural orbital is a
measure of how much this orbital contributes to
the total one-electron density.
A natural choice is to disregard the natural orbitals
with small occupation numbers and use those with large
occupation numbers to form contracted basis functions as
\begin{equation}
\varphi_k=\sum_i c_{ki} \chi_i
\end{equation}
where $\chi_i$ is the i'th primitive basis function.

As a generalization to this approach we can
average over density
matrices from several wave functions, resulting
in basis sets of the density matrix averaged ANO type,
see for example \cite{anoI,anoII,anoIII,anoIV}.
We can view the averaging of density matrices as a sequence
of rank-1 updates in the same way as in equation~\ref{eqn:d-spectral}.
We have more update vectors than the rank of the matrix, but this
does not really change anything. The important observation is
that all $\eta$'s are positive and no information is lost
in the averaging.

The general guideline for which wave functions to include is
based on what you want to be able to describe.
All wave functions you want an accurate description of
should be included in the averaging.

As an example, let us consider the oxygen atom.
We want to be able to describe the atom by itself accurately,
thus a wave function for the atom is needed, usually at the CI level.
In molecular systems, oxygen usually has a negative charge, thus
including $\rm O^-$ is almost mandatory.
A basis set derived from these two wave function is well
balanced for the majority of systems containing oxygen.
A logical conclusion would be that you need to include a few
{\it molecular} wave functions of systems containing oxygen, but in
practice this is not necessary. This is due to the fact that
the degrees of freedom describing the orbital shape distortion
when forming bonds are virtually identical to the lowest
correlating orbitals.
On the other hand, a few molecular species have oxygen with
positive charge, thus it may be appropriate to include
$\rm O^+$ in the basis set.

A wide range of specialized basis sets can also be generated,
for example a molecular basis set describing Rydberg orbitals,
see the example in the ``Tutorials and Examples'' part,
section~\ref{TUT:sec:make_rydberg_basis_sets}.
There is a possibility to create rydberg orbitals
automatically by using the keyword
\keyword{RYDBERG}. Here all unoccupied orbitals with
negative orbital energies will be used with the associated
occupation numbers
\begin{equation}
\eta_k = e^{6.9\times(\epsilon_k/\epsilon_0-1)}
\end{equation}
where $\epsilon_k$ is the orbital energy of orbital $k$ and
$\epsilon_0$ is the lowest orbital energy of all
virtual orbitals. In order to use this option you need
to use the
\program{SCF} or \program{RASSCF} program to compute
the orbitals for a cationic system.

You need one or more wave functions,
represented by formatted orbital files,
to generate the average density matrix.
These natural orbital files can be produced by any of the
wave function generators
\program{SCF},
\program{RASSCF},
\program{MRCI} or
\program{CPF}.
You could also use
\program{MBPT2} or
\program{CASPT2}.
This approach has been used in the generation of the ANO-RCC basis sets.
Your specific requirements dictate the choice of
wave function generator, but \program{MRCI} would
be most commonly used.

You are not restricted to atomic calculations but
can mix molecular and atomic calculations freely.
The restrictions are that the name of the center, for which
you are constructing a basis set, must be the same
in all wave functions.
The center may not be ``degenerate'', i.e.
it may not generate other centers through symmetry
operations. See the description of \program{SEWARD}
on page~\pageref{UG:sec:seward}
for a more extensive discussion.
For example for $\rm O_2$ you cannot use $D_{2h}$ symmetry
since this would involve one center that is mirrored into the other.
Another restriction is, of course, that you must use the
same primitive set in all calculations.

\subsection{Dependencies}
\label{UG:sec:genano_dependencies}

\program{GENANO} needs one or more wave functions in the
form of natural orbitals. Thus you need to run one or
more of
\program{SCF},
\program{RASSCF},
\program{MRCI} or
\program{CPF}.
You could also use, for example, \program{MBPT2} or \program{CASPT2}
but this is in general not recommended.
\program{GENANO} also needs the one electron file
\file{ONEINT} and the \file{RUNFILE} generated by \program{SEWARD}.

\subsection{Files}
\label{UG:sec:genano_files}
\index{Files!GENANO}\index{GENANO!Files}

Below is a list of the files that \program{GENANO}
reads/writes.
Files \file{ONEnnn}, \file{RUNnnn} and \file{NATnnn} must be supplied to
the program.
Files \file{ANO} and \file{FIG} are generated.
File \file{PROJ} is an optional input file.

\subsubsection{Input files}

\begin{filelist}
%---
\item[RUNnnn]
This file contains miscellaneous information for the nnn'th
wave function,
generated by the program {\prgmfont SEWARD}.
One file per wave function must be supplied,
\file{RUN001}, \file{RUN002}, \ldots.
%---
\item[ONEnnn]
This file contains the one-electron integrals corresponding to
the nnn'th wave function, generated by the program {\prgmfont SEWARD}.
One file per wave function must be supplied,
\file{ONE001}, \file{ONE002}, \ldots.
%---
\item[NATnnn]
This file contains the natural orbitals corresponding to the
nnn'th wave function, generated by the appropriate wave function
generating program.
One file per wave function must be supplied,
\file{NAT001}, \file{NAT002}, \ldots
%---
\item[PROJ]
This file contains orbitals used for projection of the densities.
Needs to be available if the keyword \keyword{PROJECT}
is specified.
It is compatible in format with the file \file{ANO}, and can thus be the
the file \file{ANO} from a previous run of \program{GENANO}.
\end{filelist}

\subsubsection{Output files}

\begin{filelist}
%---
\item[FIG]
This file contains a PostScript figure file of eigenvalues.
%---
\item[ANO]
This file contains the contraction coefficient matrix organized
such that each column correspond to one contracted basis function.
%---
\end{filelist}

\subsection{Input}
\label{UG:sec:genano_input}

The input file must contain the line
\begin{inputlisting}
 &GENANO
\end{inputlisting}
right before the actual input starts. Below is a list of the available keywords.
Please note that you can not abbreviate any keyword.

\begin{keywordlist}
%---
\item[TITLE]
%%Keyword: title basic
%%+This keyword starts the reading of title lines,
%%+with no limit on the number of title lines.
%%+Reading the input as title lines is stopped as soon
%%+an the input parser detects one of the other keywords.
%%+This keyword is optional.
This keyword starts the reading of title lines,
with no limit on the number of title lines.
Reading the input as title lines is stopped as soon
an the input parser detects one of the other keywords.
This keyword is {\it optional}.
%---
\item[SETS]
%%Keyword: sets basic
%%+This keyword indicates that the next line of input
%%+contains the number of sets to be used in the
%%+averaging procedure.
%%+This keyword must precede keyword WEIGHTS if
%%+both are supplied.
%%+This keyword is optional, with one set as the default.
This keyword indicates that the next line of input
contains the number of sets to be used in the
averaging procedure.
This keyword must precede \keyword{WEIGHTS} if
both are supplied.
This keyword is {\it optional}, with one set as the default.
%---
\item[CENTER]
%%Keyword: center basic
%%+This keyword is followed, on the next line, by the atom
%%+label for which the basis set is to be generated.
%%+The label must match the label you supplied to
%%+SEWARD.
%%+In previous versions of GENANO this label had to
%%+be in uppercase, but this restriction is now lifted and
%%+the case does not matter.
%%+This keyword is compulsory.
This keyword is followed, on the next line, by the atom
label for which the basis set is to be generated.
The label must match the label you supplied to
\program{SEWARD}.
In previous versions of \program{GENANO} this label had to
be in uppercase, but this restriction is now lifted and
the case does not matter.
This keyword is {\it compulsory}.
%---
\item[ROWWISE]
%%Keyword: rowwise advanced
%%+This keyword makes GENANO to produce the
%%+contraction coefficients row-wise instead of
%%+column-wise as is the default.
%%+This keyword is optional.
This keyword makes \program{GENANO} produce the
contraction coefficients row-wise instead of
column-wise as is the default.
This keyword is {\it optional}.
%---
\item[WEIGHTS]
%%Keyword: weights basic
%%+This keyword must be subsequent to keyword SETS
%%+if both are supplied.
%%+This keyword is optional,
%%+with equal weight on each of the sets as default.
This keyword must be subsequent to keyword \keyword{SETS}
if both are supplied.
This keyword is {\it optional},
with equal weight on each of the sets as default.
%---
\item[PROJECT]
%%Keyword: project advanced
%%+This keyword states that you want to project out certain
%%+degrees of freedom from the density matrix.
%%+This can be useful for generating, for example,
%%+nodeless valence orbitals to be used with ECP's.
%%+If this keyword is specified, you must supply the file
%%+PROJ obtained as file ANO from a previous
%%+GENANO calculation, for instance.
%%+This keyword is optional.
This keyword states that you want to project out certain
degrees of freedom from the density matrix.
This can be useful for generating, for example,
node less valence orbitals to be used with ECP's.
If this keyword is specified, you must supply the file
\file{PROJ} obtained as file \file{ANO} from a previous
\program{GENANO} calculation, for instance.
This keyword is {\it optional}.
%---
\item[LIFTDEGENERACY]
%%Keyword: liftdegeneracy advanced
%%+This keyword will modify the occupation numbers read from
%%+the orbitals files. The purpose is to lift the
%%+degeneracy of core orbitals to avoid rotations.
%%+The occupation numbers are changed according to
%%+o'=o*(1+10^-3/n)
%%+where n is the sequence number of the orbital
%%+in its irreducible representation.
%%+This keyword is optional.
This keyword will modify the occupation numbers read from
the orbitals files. The purpose is to lift th
degeneracy of core orbitals to avoid rotations.
The occupation numbers are changed according to
$\eta'=\eta\times(1+10^{-3}/n)$
where $n$ is the sequence number of the orbital
in its irreducible representation.
This keyword is {\it optional}.
%---
\item[RYDBERG]
%%Keyword: rydberg advanced
%%+This keyword enables automatic generation of rydberg orbitals.
%%+With this keyword all occupied orbitals will get occupation
%%+number zero while the virtual orbitals will get a small
%%+occupation number decreasing with orbital number. Useful
%%+with a calculation on an cation where the virtual orbitals
%%+are near perfect rydberg orbitals. Note that you must use
%%+orbitals from the SCF or RASSCF program.
%%+This keyword is optional.
This keyword enables automatic generation of rydberg
orbitals. With this keyword all occupied orbitals
will get occupation number zero while the virtual
orbitals will get a small occupation number
decreasing with orbital number. Useful with a calculation
on an cation where the virtual orbitals are near perfect
rydberg orbitals.
Note that you must use orbitals from the
\program{SCF} or
\program{RASSCF} program.
This keyword is {\it optional}.
%---
\item[NOTHRESHOLD]
%%Keyword: nothreshold advanced
%%+This keyword is used to specify the threshold for
%%+keeping NO's (natural orbitals). Orbitals with
%%+occupation numbers less than the threshold are
%%+discarded. The threshold is read from the line
%%+following the keyword. Default value is 1.0d-8.
This keyword is used to specify the threshold for
keeping NO's (natural orbitals). Orbitals with
occupation numbers less than the threshold are
discarded. The threshold is read from the line
following the keyword. Default value is 1.0d-8.
%---
\end{keywordlist}

Below is a simple input example, where we construct an
ANO basis set for the carbon atom.
Two wave functions are used, the SCF wave function and the
SDCI wave function for the ground state of the atom.

%%%To_extract{/doc/samples/ug/GENANO.input}
\begin{inputlisting}
 &SEWARD
Title
 Carbon atom
Symmetry
x y z
Expert
Basis set
C..... / inline
  6.0 2
   10   10
5240.6353 782.20479 178.35083 50.815942 16.823562 6.1757760 2.4180490
.51190000 .15659000 .05480600
1. 0. 0. 0. 0. 0. 0. 0. 0. 0.
0. 1. 0. 0. 0. 0. 0. 0. 0. 0.
0. 0. 1. 0. 0. 0. 0. 0. 0. 0.
0. 0. 0. 1. 0. 0. 0. 0. 0. 0.
0. 0. 0. 0. 1. 0. 0. 0. 0. 0.
0. 0. 0. 0. 0. 1. 0. 0. 0. 0.
0. 0. 0. 0. 0. 0. 1. 0. 0. 0.
0. 0. 0. 0. 0. 0. 0. 1. 0. 0.
0. 0. 0. 0. 0. 0. 0. 0. 1. 0.
0. 0. 0. 0. 0. 0. 0. 0. 0. 1.
    6    6
18.841800 4.1592400 1.2067100 .38554000 .12194000 .04267900
1. 0. 0. 0. 0. 0.
0. 1. 0. 0. 0. 0.
0. 0. 1. 0. 0. 0.
0. 0. 0. 1. 0. 0.
0. 0. 0. 0. 1. 0.
0. 0. 0. 0. 0. 1.
    3    3
1.2838000 .34400000 .09220000
1. 0. 0.
0. 1. 0.
0. 0. 1.
C  0.000000  0.000000  0.000000
End of basis

 &SCF
Occupied =  2 0 0 0 0 0 0 0

 &RASSCF
Symmetry =  4
Spin     =  3
nActEl   =  2 0 0
Frozen   =  0 0 0 0 0 0 0 0
Inactive =  2 0 0 0 0 0 0 0
Ras2     =  0 1 1 0 0 0 0 0
LevShft  =  0.00
LumOrb
Thrs     =  0.1d-8 0.1d-4 0.1d-4

 &MOTRA
LumOrb
Frozen   =  1 0 0 0 0 0 0 0

 &GUGA
Electrons =  4
Spin      =  3
Inactive  =  1 0 0 0 0 0 0 0
Active    =  0 1 1 0 0 0 0 0
CiAll     =  4

 &MRCI
SDCI

>>COPY $Project.RunFile RUN001
>>COPY $Project.RunFile RUN002
>>COPY $Project.OneInt  ONE001
>>COPY $Project.OneInt  ONE002
>>COPY $Project.RasOrb  NAT001
>>COPY $Project.CiOrb   NAT002

 &GENANO
Title
 Carbon atom
Project
sets
 2
Center
C
Weights
 0.5 0.5
>>RM ONE001
>>RM ONE002
>>RM NAT001
>>RM NAT002
\end{inputlisting}
%%%To_extract
