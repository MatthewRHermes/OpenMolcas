% environment.tex $ this file belongs to the Molcas repository $
\chapter{The \molcas\ environment}
%%%<MODULE NAME="ENVIRONMENT" LEVEL="HIDDEN">
%%Description:
%%+List of environment variables
This section describes how to use the \molcas\ program system. The
reader is assumed to be familiar with the workings of the operating
system, and only issues that are \molcas\ specific will be covered.

\section{Overview}

\molcas\ contains a set of ab initio quantum chemical programs. These
programs are essentially separate entities, but they are tied
together by a shell. The exchange of information between the programs
is through files. The shell is designed to allow ease of use with a
\hbox{minimum} amount of specifications in a `run of the mill' case. The
shell is flexible and allows the user to perform any calculation
possible within the limitations of the various codes supplied with
\molcas.

To make a calculation using \molcas\ you have to decide on which
programs you need to use, prepare input for these, and construct a
command procedure file to run the various programs. This command
procedure file is submitted for batch execution. The following two
subsections describe the programs available and the files used in
\molcas.

\subsection{Programs in the system}
\label{UG:sec:progs_list}
Below is a list of the available programs given. The programs are tied
together with a shell and the inter-{}program information is passed
through files. These files are also specified in this list to indicate
the program module interdependencies.

\begin{programlist}
%-------
\item[ALASKA]
This program computes the first derivatives of the one-{} and
two-{}electron integrals with respect to the nuclear positions.
The derivatives are not stored on files, but contracted
immediately with the one-{} and two-{}electron densities to form the
molecular gradients.
%-------
%\item[AUTO]
%This is a shell-script but is implemented into the
%\molcas\ framework as if it is a program.
%In oder to avoid writing shell scripts for each job, this script
%contains all logics required to perform any type of calculation
%using a single input file and will invoke all programs.
%-------
\item[CASPT2]
This program computes the second order Many Body Perturbation Theory
correction to CASSCF or RASSCF wave function.
%-------
\item[CASVB]
This program performs various types of valence bond calculations.
It may be called directly (for VB interpretation of CASSCF wave functions),
or within the RASSCF program (for fully variational VB calculations).
In the former case it requires the information in the
\file{JOBIPH} file generated by the \program{RASSCF} program, possibly also the
integral files \file{ONEINT} and \file{ORDINT}.
%-------
\item[CCSDT]
This program performs the iterative ROHF CCSD procedure,
optionally followed by the (T) calculation contribution.
It requires the \file{JOBIPH} file produced by
\program{RASSCF}, and \file{TRAONE} and \file{TRAINT} files produced by \program{MOTRA}.
%-------
\item[CHCC]
This program performs Closed-Shell Coupled-Clusters Singles and
Doubles calculations based exclusively on the Cholesky (or RI)
decomposed 2-electron integrals.
\item[CMOCORR]
This program compares the orbital spaces of two orbitals files.
%-------
\item[CPF]
This program produces a CPF, MCPF or ACPF wave function from a
single reference configuration.
%-------
\item[DYNAMIX]
 This program allows to do molecular dynamics
 simulations using the velocity Verlet algorithm. It has
 also the capability to detect non-adiabatic transition
 using a surface hopping algorithm.
%-------
\item[ESPF]
 The ElectroStatic Potential Fitted (ESPF) method adds some
 one-electron operators to the one-electron hamiltonian in
 order to compute the interaction between the charge
 distribution and any kind of external electrostatic
 potential, field, and field derivatives.
%-------
\item[EXPBAS]
This program takes one orbital file generated with a smaller basis set (ANO) and
expands it to a larger basis set.
%-------
\item[GATEWAY]
This program collects all information about computed system, including
geometry, basis sets and symmetry, and stores the data for a future use.

%-------
\item[GENANO]
This program is used to construct ANO type basis sets.

%-------
\item[GRID\_IT]
This program calculates densities and molecular orbitals
in a set of cartesian grid points, and produce a file for
visualisation of MO's and densities.
%-------
%\item[GV]
%GUI code for visualization and manipulation of molecular structures, and
%for visualization of molecular orbitals, densities, density differences, etc.
%-------
\item[FFPT]
This program applies perturbations to the one-{}electron Hamiltonian
for finite field perturbation calculations.
%-------
\item[GUGA]
This program generates the coupling coefficients required by the
\program{MRCI} and
\program{CPF} programs.
%-------
\item[LOCALISATION]
This program generates localised occupied orbitals according to one of
 the following procedures: Pipek-Mezey, Boys,
 Edmiston-Ruedenberg, or Cholesky. Orthonormal, linearly
 independent, local virtual orbitals may also be generated
 from projected atomic orbitals (Cholesky PAOs).
%-------
\item[LOPROP]
 This program computes molecular properties based on the
 one-electron density or transition-density and
 one-electron integrals like charges, dipole moments and
 polarizabilities
%-------
\item[MBPT2]
This program computes the second order Many Body Perturbation Theory
correction to an SCF wave function.
%-------
\item[MCKINLEY]
This program calculates the second and first order derivatives of
integrals that are used
for calculating second order derivatives of the energies with perturbation
dependent basis sets.
%-------
\item[MCLR]
This program calculates the response of the wave function
and related second order properties.
%-------
\item[MOTRA]
This program transforms one- and two-electron integrals from AO
basis to MO basis. The integrals that are transformed are the
one-electron Hamiltonian and the two-electron repulsion integrals.
%-------
\item[MRCI]
This program produces a Multi Reference CI wave function from an
arbitrary set of reference configurations. Alternatively the program
can produce an Averaged CPF wave function.
%-------
\item[MULA]
This program calculates intensities of vibrational
transitions between electronic states.
%-------
\item[NEMO]
A set of computational modules for calculation of
interaction energies between molecules.
%-------
\item[RASSCF]
This program generates CASSCF, RASSCF and GASSCF type wave functions.
%-------
\item[RASSI]
This program computes the interaction between several RASSCF
wave functions. A spin-orbit Hamiltonian can be used.
%-------
\item[SCF]
This program generates Closed Shell SCF or Unrestricted SCF,
and Kohn-Sham DFT wave functions.
%-------
\item[SEWARD]
This program generates one-{} and two-{}electron integrals needed
by other programs. If requested the two-electron integrals are
computed in the form of Cholesky decomposed vectors.
%---------
\item[SINGLE\_ANISO]
This program allows the non-perturbative
 calculation of effective spin (pseudospin) Hamiltonians
 and static magnetic properties of mononuclear complexes
 and fragments completely ab initio, including the
 spin-orbit interaction.
%---------
\item[SLAPAF]
This program is a general purpose facility for geometry
optimization, transition state search, MEP, conical intersections, intersystem
crossings, etc. using analytical or numerical gradients produced by
\program{ALASKA}.
%-------
\item[VIBROT]
This program computes the vibrational-{}rotational spectrum of a
diatomic molecule. Spectroscopic constants are computed. The program can also
compute transition moments, life times , etc for excited state potentials.
%-------
\end{programlist}

\subsection{Files in the system}
\label{UG:sec:files_list}

The following is a list of the most common files in \molcas\ that are used
to exchange information between program modules. The names given in
this list are the FORTRAN file names, defined in the source code.
Actual file names are constructed from so called prgm tables, specific
for each individual module. (More information about PRGM files can be found
in the Molcas Programming Guide).

\begin{filelist}
%----
\item[RUNFILE]
This file contains general information of the calculation. All programs
read from it and write to it. \program{GATEWAY} creates a new \file{RUNFILE}
corresponding to a new calculation.
%-----
\item[ONEINT]
This file contains the one-electron integrals generated by the program
\program{SEWARD}.
%-----
\item[ORDINT]
This file contains the ordered two-electron integrals generated by the program
\program{SEWARD}.
%-----
\item[RYSRW]
Data base for the fast direct evaluation of roots and weights of
the Rys polynomials. This file is a part of the program system and
should not be manipulated by the user.
\item[ABDATA]
Data base for the evaluation of roots and weights of high order Rys
polynomial. This file is a part of the program system and should
not be manipulated by the user.
%-----
\item[CHVEC]
This file contains the Cholesky vectors representing the two-electron integrals
as generated by the program
\program{SEWARD}.
%-----
\item[CHORST]
This file contains information about each of the Cholesky
vectors generated by the program
\program{SEWARD}.
%-----
\item[CHRED]
This file contains information about the storage mode of the Cholesky
vectors generated by the program
\program{SEWARD}.
%-----
\item[CHOR2F]
File containing the mapping between Cholesky vector storage and the canonical orbital ordering.
%-----
\item[TRAINT]
This file contains the transformed two-electron integrals generated
by the program
\program{MOTRA}.
%-----
\item[TRAONE]
This file contains the transformed one-electron integrals generated by
the program
\program{MOTRA}.
%-----
\item[INPORB] A generic name for an orbital file. Different programs
uses and/or generates \file{INPORB} files with a specific name:
\begin{itemize}
\item[GSSORB] generated by the program \program{GUESSORB}.
%-----
\item[SCFORB] generated by the program \program{SCF}.
%-----
\item[RASORB] generated by the program \program{RASSCF}.
%-----
\item[CIORB] generated by the program \program{MRCI}
%-----
\item[CPFORB] generated by the program \program{CPF}.
%-----
\item[SIORB] generated by the program \program{RASSI}.
%-----
\item[PT2ORB] generated by the program \program{CASPT2}.
%-----
\end{itemize}
\item[JOBIPH]
This file contains the RASSCF wave function information generated by the
\program{RASSCF} program.
%-----
\item[JOBOLD]
This file contains the RASSCF wave function information generated by the
\program{RASSCF} program in the file \file{JOBIPH}, and is used as input for a
 subsequent \program{RASSCF} calculation.
\item[JOBMIX]
This file contains the multi-state CASPT2 wave function information
generated by the
\program{CASPT2} program, and is used as input for a
 subsequent \program{RASSI} calculation.
\item[GRID]
This file contains binary or ASCII data generated by
\program{GRID\_IT} program for visualization of density or
molecular orbitals.
%-----
\end{filelist}

\section{Commands and environment variables}

This section will describe the usage of \molcas\ in an
UNIX environment.

Production jobs using \molcas\ in an UNIX environment can be
performed as batch jobs.
This requires the creation of a shell script that
contains a few simple commands. Further you need to create input for
each program module that you intend to use. This section describes the
necessary steps you have to take in order to make a successful job using
\molcas.
Input examples for a typical \molcas\
run can be found in \file{doc/samples/problem\_based\_tutorials/} directory.
Also you can use some input examples
in \file{Test/input} subdirectory.

\subsection{Commands}

There is a command supplied with the \molcas\ package, named
\command{molcas}, that the user issue to perform a given task.
A sequence of such commands will perform the calculation requested by
the user.
\begin{commandlist}
%---
\item[molcas]
This command tells which \molcas\ installation will be used, and gives
some help about usage of \molcas\ command
%---
\item[molcas \file{input-file}]
This command executes a command in the \molcas\ system.
%---
\item[molcas help \program{prgm}]
This command gives the list of available keywords for program \program{prgm}.
%---
\item[molcas help \program{prgm keyword}]
This command gives description of a \program{keyword}.
%---
\item[molcas help \program{environment}]
This command gives a list of \molcas\ specific environment variables.

%---
\item[molcas help \program{basis element}]
This command gives a list of basis sets available for an \program{element}.


%---
\end{commandlist}

The following is an example of running \molcas\ by using a single input file:
\begin{sourcelisting}
molcas $Project.input
\end{sourcelisting}

%$


An alternative way of running \molcas\ as a sequence of separate calls:
\begin{sourcelisting}
molcas $Project.seward.input    # Execute seward
molcas $Project.scf.input       # Execute scf
\end{sourcelisting}

By default, the output will go directly to the screen. If can be redirected
by using flag -f, e.g. \command{molcas -f water.inp} will store the output
in \file{water.log} and \file{water.err} files.

The default behavior of \molcas\ execution can be altered by setting environment variables.


\subsection{Project name and working directory}

When running a project, \molcas\ uses the variable
\variable{Project} giving a project name, and a scratch directory defined by
the variable \variable{WorkDir}.
This serves the purpose of maintaining structure of the files and
facilitating automatic file mapping.

There are several ways to set up these variables.
By default, the name of the Project constructed from the name of the input file,
by removing the last suffix, e.g. for example for an input name \file{Water.SCF.input}
the \variable{Project} name will be \variable{Water.SCF}.
Alternatively,
user can set environment variable \variable{Project}, or \variable{MOLCAS\_PROJECT}.

Scratch directory can be set by environment variable \variable{MOLCAS\_WORKDIR}.
If it is set to value "PWD", current directory will be used. Otherwise,
it can be set to a directory name. In this case scratch area will be located
in a subdirectory \$MOLCAS\_WORKDIR/\$Project. It is also possible to
overwrite the value of scratch area, by setting environment variable
\variable{WorkDir}.

\begin{itemize}
\itemsep 9pt plus 3pt minus 3pt
\item \command{Project=$\ldots$; export Project}
\item \command{WorkDir=$\ldots$; export WorkDir}
\end{itemize}

\molcas\ modules communicates between each other via files, located in the \variable{WorkDir}.
The description of internal filenames and file mapping can be found at Appendix.

\subsection{Input}

When you have decided which program modules you need to use to perform your
calculation, you need to construct input for each of these. There is no
particular structure enforced on the input files, but it is recommended that
you follow:
\begin{itemize}
\item \file{\$Project.{\rm``}prgm-name{\rm''}.input}
\end{itemize}
which is the name of the input files assumed in the sample shell script.


\subsection{Preparing a job}

When you prepare a job for batch processing, you have to create a shell script.
It is recommended that you use the sample shell script supplied with
\molcas\ as a starting point when building your own shell script.
The following steps are taken in the shell script:
\begin{enumerate}
%\itemsep 9pt plus 3pt minus 3pt
\item
Define and export the \molcas\ variables
\begin{itemize}
\item Project (or use $MOLCAS\_PROJECT$)
\item WorkDir (or $MOLCAS\_WORKDIR$)
\end{itemize}
\item
Issue a sequence of \molcas\ commands.
\item
Remove the scratch directory and all files in it.
\end{enumerate}



The following is an example of a shell script.
\begin{sourcelisting}
Project=HF; export Project                               # Define the project id
WorkDir=/temp/$LOGNAME/$Project.$RANDOM; export WorkDir  # Define scratch directory
molcas $Project.input                                    # Run molcas with input file, which
                                                         #   contains inputs for several modules
rm -r $WorkDir                                           # Clean up
\end{sourcelisting}

%$
The file \file{\$ThisDir/\$Project.input} contains the ordered sequence
of \molcas\ inputs and the EMIL interpreter will call the appropriate
programs. See section~\ref{UG:sec:EMIL} for an explanation of the
additional tools available in the EMIL interpreter.

The following is an example of a shell script to be submitted for batch
execution.
\begin{sourcelisting}
Project=HF; export Project                               # Define the project id
WorkDir=/temp/$LOGNAME/$Project.$RANDOM; export WorkDir  # Define scratch directory
molcas $Project.seward.input                             # Execute seward
molcas $Project.scf.input                                # Execute scf
rm -r $WorkDir                                           # Clean up
\end{sourcelisting}

An alternative way to control teh usage of the WorkDir is to use flags in molcas command:

\begin{variablelist}
\item[-new]
clean WorkDir before the usage

\item[-clean]
clean WorkDir after the usage
\end{variablelist}

Note, that if you configured your working environment by using \command{setuprc} script,
the only command you have to place into the shell script is:
\begin{sourcelisting}
molcas $Project.input
\end{sourcelisting}
%$

\subsection{System variables}
\label{UG:sec:sysvar}

\molcas\ contains a set of system variables that the user can
set to modify the default behaviour of \molcas. Two of them
(Project and WorkDir) must be set in order to make \molcas\ work at all.
There are defaults for these but you are advised not to use the defaults.

There are several ways of using \molcas\ environment variables:
\begin{itemize}
\item These variables can be exported in your shell script
\begin{sourcelisting}
export MOLCAS_MEM=512
molcas input
\end{sourcelisting}
\item These variables can be included into \molcas\ input:
\begin{sourcelisting}
* begin of the input file
>>> export MOLCAS_MEM=512

  . . .
\end{sourcelisting}
\item variables can be included directly into \command{molcas} command in the form:
\begin{sourcelisting}
molcas MOLCAS_MEM=512 input
\end{sourcelisting}
\end{itemize}

The simplest way to set up default environment for \molcas\ is
to use script \file{setuprc}, which can be run as command
\command{molcas setuprc}. This interactive script creates
a resource file \file{molcasrc}, located either in $\$MOLCAS$ or $\$HOME/.Molcas$
%$
directory. The priority of these settings is: user defined settings
(e.g. in \command{molcas} command), user resource file, \molcas\ resource file.

Two flags in \molcas\ command are related to resource files:
\begin{variablelist}
\item[-env]
Display current \molcas\ environment

e.g. \command{molcas -env input} will print information about environment
variables, used during execution of the input file.

\item[-ign]
Ignore resource files

e.g. \command{molcas -ign input} will process input file without settings,
which are stored in $\$MOLCAS/molcasrc$ and in $\$HOME/molcasrc$ files.
%$
\end{variablelist}


The most important environment variables, used in \molcas:
\begin{variablelist}
%---
\item[Project]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="Project" APPEAR="Project" KIND="STRING" LEVEL="ADVANCED">
%%Keyword: Project <advanced>
%%%<HELP>
%%+This variable can be set in order to overwrite the default name of the
%%+project you are running. The default (and recommended) value of the project name
%%+is the name of the input file (without the file extension).
%%%</HELP></KEYWORD>
This variable can be set in order to overwrite the default name of the
project you are running. The default (and recommended) value of the project name is the
name of the input file (without the file extension).
%---
\item[WorkDir]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="WorkDir" APPEAR="WorkDir" KIND="STRING" LEVEL="ADVANCED">
%%Keyword: WorkDir <advanced>
%%%<HELP>
%%+This variable can be used to specify directly the directory where all files
%%+that molcas creates are placed. See MOLCAS_WORKDIR for more options.
%%%</HELP></KEYWORD>
This variable can be used to specify directly the directory where all files
that \molcas\ creates are placed. See \keyword{MOLCAS\_WORKDIR} for more options.
%---
\item[CurrDir]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="CurrDir" APPEAR="CurrDir" KIND="STRING" LEVEL="ADVANCED">
%%Keyword: CurrDir <advanced>
%%%<HELP>
%%+This variable corresponds to the location of the input, and it is used as
%%+a default location for all output files, generated by molcas modules.
%%%</HELP></KEYWORD>
This variable corresponds to the location of the input, and it is used as
a default location for all output files, generated by \molcas modules.
%---
\item[MOLCAS]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS" APPEAR="MOLCAS" KIND="STRING" LEVEL="ADVANCED">
%%Keyword: MOLCAS <advanced>
%%%<HELP>
%%+This variable indicates the location of molcas. The default version of Molcas
%%+to be used is specified at file .Molcas/molcas, located at user HOME directory.
%%%</HELP></KEYWORD>
This variable indicates the location of \molcas. The default version of Molcas
to be used is specified at file \file{.Molcas/molcas}, located at user HOME directory.
%---
\item[MOLCAS\_NPROCS]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_NPROCS" APPEAR="MPI Processes" KIND="STRING" LEVEL="BASIC">
%%Keyword: MOLCAS_NPROCS <basic>
%%%<HELP>
%%+This variable should be used to run molcas code in parallel. It defines the
%%+number of computational units (cores or nodes) which will be used.
%%%</HELP></KEYWORD>
This variable should be used to run \molcas code in parallel. It defines the
number of computational units (cores or nodes) which will be used.
%---
\item[MOLCAS\_MEM]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_MEM" APPEAR="MOLCAS_MEM (Mb)" KIND="INT" LEVEL="BASIC">
%%Keyword: MOLCAS_MEM <basic>
%%%<HELP>
%%+This environment variable controls the size (soft limit) of the
%%+work array (in Mb) utilized in the programs that offer dynamic memory.
%%+It is also possible to set up memory in Gb, e.g. 2Gb
%%%</HELP></KEYWORD>
This environment variable controls the size (soft limit) of the
work array utilized in the programs that offer dynamic memory.
It is specified in Megabytes, i.e. \\
\command{MOLCAS\_MEM=256; export MOLCAS\_MEM}\\
will assign 256Mb for the working arrays.
It is also possible to use Gb (Tb) to specify memory in Gb or Tb.
\begin{itemize}
\item MOLCAS\_MEM is undefined --- The default amount of memory (1024MB),
will be allocated for the work arrays.
\item MOLCAS\_MEM is defined but nonzero --- This amount of memory
will be allocated.
\end{itemize}
See also \keyword{MOLCAS\_MAXMEM}.
%---
\end{variablelist}


The complete list of \molcas-related environment variables:
\begin{variablelist}
%---
\item[MOLCAS\_COLOR]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_COLOR" APPEAR="Use markup in the output" KIND="CHOICE" LIST="----,NO" LEVEL="ADVANCED">
%%Keyword: MOLCAS_COLOR <advanced>
%%%<HELP>
%%+By default molcas uses markup characters in the output.
%%+To overwrite, set the key to NO
%%%</HELP></KEYWORD>
By default molcas uses markup characters in the output. To overwrite, set the key to NO.
%---
\item[MOLCAS\_NPROCS]
See above
%---
\item[MOLCAS\_DEBUGGER]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_DEBUGGER" APPEAR="Debugger" KIND="STRING" LEVEL="ADVANCED">
%%Keyword: MOLCAS_DEBUGGER <advanced>
%%%<HELP>
%%+This variable can be set to the name of debugger (or another code) which will be used on top of
%%+molcas executables. The option is useful for tracing an error in the code
%%%</HELP></KEYWORD>
This variable can be set to the name of debugger (or another code) which will be used on top of
molcas executables. The option is useful for tracing an error in the code
%---
\item[MOLCAS\_DISK]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_DISK" APPEAR="MOLCAS_DISK" KIND="INT" LEVEL="ADVANCED">
%%Keyword: MOLCAS_DISK <advanced>
%%%<HELP>
%%+The value of this variable is used to split large files into a set of
%%+smaller datasets, as many as are needed (max. 20 subsets).
%%%</HELP></KEYWORD>
The value of this variable is used to split large files into a set of
smaller datasets, as many as are needed (max. 20 subsets). It is specified
in Megabytes, for instance, \command{MOLCAS\_DISK=1000; export MOLCAS\_DISK},
and the following rules apply:
\begin{itemize}
\item MOLCAS\_DISK is undefined --- The program modules will ignore this
option and the file size limit will be defined by your hardware
(2 GBytes for 32-bit machines).
\item MOLCAS\_DISK=0 (zero) --- The programs will assume a file size limit
of 2 GBytes (200GBytes on 64-bit machines).
\item MOLCAS\_DISK is defined but nonzero --- The files will be limited to
this value (approximately) in size.
\end{itemize}
%---
\item[MOLCAS\_ECHO\_INPUT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_ECHO_INPUT" APPEAR="Echo input" KIND="CHOICE" LIST="----,NO" LEVEL="ADVANCED">
%%Keyword: MOLCAS_ECHO_INPUT <advanced>
%%%<HELP>
%%+An environment variable to control echoing of the input.
%%+To suppress print level, set MOLCAS_ECHO_INPUT to 'NO'.
%%%</HELP></KEYWORD>
An environment variable to control echoing of the input.
To suppress print level, set MOLCAS\_ECHO\_INPUT to 'NO'.

\item[MOLCAS\_FIM]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_FIM" APPEAR="FiM" KIND="CHOICE" LIST="----,YES" LEVEL="ADVANCED">
%%Keyword: MOLCAS_FIM <advanced>
%%%<HELP>
%%+Activate the Files in Memory I/O layer
%%%</HELP></KEYWORD>
Activates the Files In Memory I/O layer. See section \ref{MT:sec:fim} for more details.
{\it Note that this setting is available only in MOLCAS compiled without Global
Arrays}.
%---

%---
\item[MOLCAS\_LICENSE]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_LICENSE" APPEAR="License Directory" KIND="DIR" LEVEL="ADVANCED">
%%Keyword: MOLCAS_LICENSE <advanced>
%%%<HELP>
%%+An environment which specifies the directory with molcas license file license.dat.
%%+The default value of this variable is directory .Molcas/ in user home directory.
An environment which specifies the directory with \molcas\ license file \file{license.dat}.
The default value of this variable is directory \file{.Molcas/} in user home directory.
%%%</HELP></KEYWORD>
%---
\item[MOLCAS\_LINK]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_LINK" APPEAR="Link information" KIND="CHOICE" LIST="YES,NO" LEVEL="ADVANCED">
%%Keyword: MOLCAS_LINK <advanced>
%%%<HELP>
%%+An environment variable to control information about linking of files.
%%+By default (MOLCAS_LINK is not set) only essential
%%+information about linking will be printed. To increase/decrease the
%%+print level, set MOLCAS_LINK to 'Yes'/'No'.
%%%</HELP></KEYWORD>
An environment variable to control information about linking of files.
By default (MOLCAS\_LINK is not set) only essential
information about linking will be printed. To increase/decrease the
print level, set MOLCAS\_LINK to 'Yes'/'No'.

%---
\item[MOLCAS\_MAXITER]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_MAXITER" APPEAR="Max Iter" KIND="INT" LEVEL="ADVANCED">
%%Keyword: MOLCAS_MAXITER <advanced>
%%%<HELP>
%%+An environment variable to control maximum number of iterations in DO WHILE loop
%%%</HELP></KEYWORD>
An environment variable to control maximum number of iterations in DO WHILE loop.
%---

\item[MOLCAS\_MAXMEM]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_MAXMEM" APPEAR="Max Memory" KIND="INT" LEVEL="ADVANCED">
%%Keyword: MOLCAS_MAXMEM <advanced>
%%%<HELP>
%%+An environment variable to set up a hard limit for allocated memory (in Mb).
%%+If is not specified, then it takes value of MOLCAS_MEM. Otherwise, the
%%+(MOLCAS_MAXMEM-MOLCAS_MEM) amount of RAM will be primarily used for keeping
%%+files in memory (FiM), or allocating Distributed Global Arrays. Note that
%%+this setting is available only in MOLCAS compiled without GA.
%%%</HELP></KEYWORD>
An environment variable to set up a hard limit for allocated memory (in Mb).
If is not specified, then it takes value of MOLCAS\_MEM. Otherwise, the (MOLCAS\_MAXMEM-MOLCAS\_MEM)
amount of RAM will be primarily used for keeping files in memory (FiM), or allocating Distributed Global Arrays.
{\it Note that this setting is available only in MOLCAS compiled without Global Arrays}.
%---
\item[MOLCAS\_MEM]
See above.
%---
\item[MOLCAS\_MOLDEN]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_MOLDEN" APPEAR="MOLDEN output" KIND="CHOICE" LIST="ON,OFF" LEVEL="ADVANCED">
%%Keyword: MOLCAS_MOLDEN <advanced>
%%%<HELP>
%%+If MOLCAS_MOLDEN set to 'ON' a Molden style input file will be generated regardless of the number of orbitals.
%%%</HELP></KEYWORD>
If MOLCAS\_MOLDEN set to 'ON' a Molden style input file will be generated regardless of the number of orbitals.
%---
\item[MOLCAS\_NEW\_WORKDIR]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_NEW_WORKDIR" APPEAR="Use new WorkDir" KIND="CHOICE" LIST="YES" LEVEL="BASIC">
%%Keyword: MOLCAS_NEW_WORKDIR <basic>
%%%<HELP>
%%+If set to YES molcas will never reuse files in scratch area.
%%+This setting can be overwritten by running molcas with flag -old:
%%+ molcas -old input
%%%</HELP></KEYWORD>
If set to YES \molcas\ will never reuse files in scratch area.
This setting can be overwritten by running \command{molcas} with flag \command{-old}:
 \command{molcas -old input}
%---
\item[MOLCAS\_OUTPUT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_OUTPUT" APPEAR="Output Directory" KIND="CHOICE" LIST="????,PWD,?DIR,NAME,WORKDIR" LEVEL="BASIC">
%%Keyword: MOLCAS_OUTPUT <basic>
%%%<HELP>
%%+This variable can alter the default directory for extra output files,
%%+such as orbitals files, molden files, etc.
%%+If set, molcas will save output files to the specified directory.
%%+The directory name can be set in the form of absolute PATH, or
%%+relative PATH (related to submit directory)
%%+A special value 'WORKDIR' will keep all output files in WorkDir.
%%+A special value 'NAME' will create a subdirectory with a name of Project.
%%+If the variable is not set, all output files will be copied or moved
%%+to the current directory.
%%+Default value can be forced by MOLCAS_OUTPUT=PWD
%%%</HELP></KEYWORD>
This variable can alter the default directory for extra output files,
such as orbitals files, molden files, etc.
If set, \molcas\ will save output files to the specified directory.
The directory name can be set in the form of absolute PATH, or
relative PATH (related to the submit directory).
A special value 'WORKDIR' will keep all output files in WorkDir.
A special value 'NAME' will create a subdirectory with a name of Project.
If the variable is not set, all output files will be copied or moved
to the current directory. Default value can be forced by MOLCAS\_OUTPUT=PWD.
%---
\item[MOLCAS\_PRINT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_PRINT" APPEAR="Print level" KIND="CHOICE" LIST="----,0:SILENT,1:TERSE,2:NORMAL,3:VERBOSE,4:DEBUG,5:INSANE" LEVEL="BASIC">
%%Keyword: MOLCAS_PRINT <basic>
%%%<HELP>
%%+MOLCAS_PRINT variable controls the level of output. The value could be numerical or mnemonic:
%%+SILENT (0), TERSE (1), NORMAL (2), VERBOSE (3), DEBUG (4) and INSANE (5).
%%%</HELP></KEYWORD>
MOLCAS\_PRINT variable controls the level of output. The value could be numerical or mnemonic:
SILENT (0), TERSE (1), NORMAL (2), VERBOSE (3), DEBUG (4) and INSANE (5).
%---
\item[MOLCAS\_PROJECT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_PROJECT" APPEAR="Project Policy" KIND="CHOICE" LIST="????,NAME,NAMEPID" LEVEL="BASIC">
%%Keyword: MOLCAS_PROJECT <basic>
%%%<HELP>
%%+If set to the value NAME, molcas will use the prefix of the input file
%%+as a project name,
%%+If set to the value NAMEPID, the Project name still will be constructed
%%+from the name of input file, however, the name of scratch area will
%%+be random
%%%</HELP></KEYWORD>
If set to value NAME, \molcas\ will use the prefix of the input file
as a project name. Otherwise, it set a project name for the calculation.
If set to the value NAMEPID, the Project name still will be constructed
from the name of input file, however, the name of scratch area will
be random.
%---
\item[MOLCAS\_PROPERTIES]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_PROPERTIES" APPEAR="Property output" KIND="CHOICE" LIST="SHORT,LONG" LEVEL="ADVANCED">
%%Keyword: MOLCAS_PROPERTIES <advanced>
%%%<HELP>
%%+If MOLCAS_PROPERTIES is set to 'LONG' properties with the individual MO contributions will be listed.
%%%</HELP></KEYWORD>
If MOLCAS\_PROPERTIES is set to 'LONG' properties with the individual MO contributions will be listed.
%---
\item[MOLCAS\_REDUCE\_PRT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_REDUCE_PRT" APPEAR="Verbose input in do loops" KIND="CHOICE" LIST="----,NO" LEVEL="ADVANCED">
%%Keyword: MOLCAS_REDUCE_PRT <advanced>
%%%<HELP>
%%+If set to NO, print level in DO WHILE loop is not reduced
%%%</HELP></KEYWORD>
If set to NO, print level in DO WHILE loop is not reduced.
%---
\item[MOLCAS\_REDUCE\_NG\_PRT]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_REDUCE_NG_PRT" APPEAR="Verbose input in numerical_gradient" KIND="CHOICE" LIST="----,NO" LEVEL="ADVANCED">
%%Keyword: MOLCAS_REDUCE_NG_PRT <advanced>
%%%<HELP>
%%+If set to NO, print level in numerical_gradient is not reduced.
%%%</HELP></KEYWORD>
If set to NO, print level in \program{numerical\_gradient} loop is not reduced.
%---
\item[MOLCAS\_SAVE]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_SAVE" APPEAR="Save files as" KIND="CHOICE" LIST="----,INCR,ORIG" LEVEL="BASIC">
%%Keyword: MOLCAS_SAVE <basic>
%%%<HELP>
%%+This variable can alter the default filenames for output files.
%%+If not set (default), all files will overwrite old files.
%%+If set to 'INCR' all output files will get an incremental
%%+filenames.
%%+If set to 'ORIG' - an existent file will be copied with
%%+an extension '.orig'
%%%</HELP></KEYWORD>
This variable can alter the default filenames for output files.
If not set (default), all files will overwrite old files.
If set to 'INCR' all output files will get an incremental
filenames.
If set to 'ORIG' - an existent file will be copied with
an extension '.orig'

%---
%---
\item[MOLCAS\_TIME]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_TIME" APPEAR="Timing" KIND="CHOICE" LIST="----,YES" LEVEL="ADVANCED">
%%Keyword: MOLCAS_TIME <advanced>
%%%<HELP>
%%+If set, switch on timing information for each module
If set, switch on timing information for each module
%%%</HELP></KEYWORD>

%---
\item[MOLCAS\_TIMELIM]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_TIMELIM" APPEAR="Time Limit" KIND="INT" LEVEL="ADVANCED">
%%Keyword: MOLCAS_TIMELIM <advanced>
%%%<HELP>
%%+Set up a timelimit for each module (in minutes). By default, the maximum
%%+execution time is set to unlimited. Note that this setting is available only
%%+in MOLCAS compiled without Global Arrays.
%%%</HELP></KEYWORD>
Set up a timelimit for each module (in minutes). By default, the maximum
execution time is set to unlimited. {\it Note that this setting is available only
in MOLCAS compiled without Global Arrays}.
%---
\item[MOLCAS\_TRAP]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_TRAP" APPEAR="Trap on Error" KIND="CHOICE" LIST="----,OFF" LEVEL="ADVANCED">
%%Keyword: MOLCAS_TRAP <advanced>
%%%<HELP>
%%+If MOLCAS_TRAP set to 'OFF' molcas modules will continue to be executed,
%%+even if a non-zero return code was produced.
%%%</HELP></KEYWORD>
If MOLCAS\_TRAP set to 'OFF' \molcas\ modules will continue to be executed,
even if a non-zero return code was produced.

\item[MOLCAS\_WORKDIR]
%%%<KEYWORD MODULE="ENVIRONMENT" NAME="MOLCAS_WORKDIR" APPEAR="Top WORKDIR" KIND="CHOICE" LIST="?DIR" LEVEL="BASIC">
%%Keyword: MOLCAS_WORKDIR <basic>
%%%<HELP>
%%+A parent directory for all scratch areas. It can be set to an
%%+absolute PATH (recommended), to a relative PATH, or to a special value PWD
%%+(to use current directory for scratch files)
%%%</HELP></KEYWORD>
A parent directory for all scratch areas. It can be set to an
absolute PATH (recommended), to a relative PATH, or to a special value PWD
(to use current directory for scratch files)
%---


\end{variablelist}

User can customize his installation by adding MOLCAS environment variable into \file{molcasrc} file.

Another way of customizing \molcas\ is to use prologue and epilogue scripts. If user created a file
\file{prologue} in \file{\$HOME/.Molcas} directory it will be executed (as ./prologue) before \molcas\ calculation
starts. \file{epilogue} in \file{\$HOME/.Molcas} directory will be executed at the end of calculation.
Files \file{module.prologue} and \file{module.epilogue} contains commands executing before and after
each executable molcas module. These files may use internal Molcas variables, such as
$\$Project$, $\$WorkDir$, $\$MOLCAS_MEM$ etc. Note that prologue/epilogue scripts should be executable.
For debug purposes, the location of prologue and epilogie files can be set by $\$MOLCAS_LOGUE_DIR$ variable.


Example:
\begin{verbatim}
prologue:
 echo Calculation of $Project input will start at `date`
module.prologue:
 echo Running module $MOLCAS_CURRENT_PROGRAM at $WorkDir
\end{verbatim}


%%%</MODULE>

%%%<MODULE NAME="COMMENT" LEVEL="HIDDEN">
%%%	<KEYWORD MODULE="COMMENT" NAME="UNDEFINED" APPEAR="Unrecognized Content" KIND="STRINGS" LEVEL="BASIC" />
%%%</MODULE>


